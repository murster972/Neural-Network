#!/usr/bin/env python3
import numpy
import math
from random import uniform, randint

class NeuralNetwork:
    ''' Represents simple multi-perceptron NN '''
    def __init__(self, layer_nodes):
        ''':param layer_nodes: list indicating number of nodes in each layer, minimum
                               of 3 items - input, one hidden and output '''
        self.layer_nodes = layer_nodes
        self.no_of_layers = len(layer_nodes)

        # activation function used: 0 - sigmoid, 1 - guassian
        self.activation_function = 1

        #TODO: use one-dimensional optimization to adjust learning rate as network
        #      is being trainned
        # learning rate used in backprop
        self.learning_rate = 0.1

        # weight and bias matrics fo each layer
        self.weight_matrices = []
        self.bias_matrices = []

        # activations for neurons on each layer
        # first item is for input layer
        self.activation_matrices = [numpy.matrix([[0]] * layer_nodes[0])]

        # weighted inputs for each layer from 1st hidden onwards
        # used in backprop - symbol z in equations
        self.weighted_inputs = [0] * (self.no_of_layers - 1)

        # from 1st item instead of 0th layer is input and doesnt layer doesnt require weights
        for layer in range(1, len(layer_nodes)):
            no_nodes = layer_nodes[layer]

            # nodes in previous layer
            prev_nodes = layer_nodes[layer - 1]

            # creates N weights for each node in current layer, where N is equal to no of nodes
            # in previous layer
            # weights = [[round(randint(0, 1) + 0.0, 0) for i in range(prev_nodes)] for weight in range(no_nodes)]
            weights = [[1 / (self.layer_nodes[0]**0.5) for i in range(prev_nodes)] for weight in range(no_nodes)]
            bias = [[0.0]] * no_nodes
            act = [[0]] * no_nodes

            self.weight_matrices.append(numpy.matrix(weights))
            self.bias_matrices.append(numpy.matrix(bias))
            self.activation_matrices.append(numpy.matrix(act))

    def train(self, trainning_data, max_epochs=10, size=500):
        ''' trains network using epochs with mini-batches
            :param trainning_data: list of inputs and their expected outputs,
                                   e.g. [([1, 0], [1]), ([0, 0], [0])] for NN with two inputs
                                   and one output '''
        # TODO: check http://neuralnetworksanddeeplearning.com/chap3.html
        #       to look at better ways to train the NN faster
        #
        #       patricularly look at cross-entropy cost function

        # https://visualstudiomagazine.com/articles/2014/08/01/batch-training.aspx
        # explains batch and online trainning

        #NOTE: check https://www.neuwell no shit :joy::raldesigner.com/blog/5_algorithms_to_train_a_neural_network

        # stops errors for small trainning_data sizes
        batch_size = size if size <= len(trainning_data) else len(trainning_data)

        # for ind in range(len(trainning_data)):
        #     item = trainning_data[ind]
        #     inputs, expected = item
        #
        #     self.__feed_forward(inputs)
        #     errors = self.__backprop(expected)[::-1]
        #
        #     print(ind)
        #
        #     for layer in range(self.no_of_layers - 1, 0, -1):
        #         for j in range(len(self.weight_matrices[layer - 1])):
        #             for k in range(len(self.weight_matrices[layer - 2])):
        #                 ak = self.activation_matrices[layer - 1][k]
        #                 s = errors[layer - 1][j]
        #
        #                 w_delta = ak * s
        #                 b_delta = s
        #
        #                 self.weight_matrices[layer - 1] -= w_delta
        #                 self.bias_matrices[layer - 1] -= b_delta


        for epoch in range(max_epochs):
            print("epoch", epoch)

            # shuffle trainning data
            # numpy.random.shuffle(trainning_data)

            batch_activations = []
            batch_errors = []

            average_cost = 0

            # get mini-batch
            for ind in range(len(trainning_data[:batch_size])):
                item = trainning_data[:batch_size][ind]
                inputs, expected = item

                self.__feed_forward(inputs)
                errors = self.__backprop(expected)

                #BUG: had activated - expected, instead of expected - activated
                #     may be causing slow learning rate
                cost = 0.5 * sum([(expected[j] - self.activation_matrices[-1][j])**2 for j in range(self.layer_nodes[-1])])
                # cost = sum([(self.activation_matrices[-1][j] - expected[j])**2 for j in range(self.layer_nodes[-1])])
                average_cost += cost
                average_cost /= (ind + 1)

                batch_activations.append([] + self.activation_matrices)
                batch_errors.append([] + errors[::-1])
            print(average_cost)

            # adjust weights based on calcs from all of mini-batch
            for layer in range(self.no_of_layers - 1, 0, -1):
                w = self.weight_matrices[layer - 1]
                b = self.bias_matrices[layer - 1]

                w_delta = sum([batch_errors[x][layer - 1] * batch_activations[x][layer - 1].transpose() for x in range(batch_size)])
                w_delta *= (self.learning_rate / batch_size)

                b_delta = sum([batch_errors[x][layer - 1] for x in range(batch_size)])
                b_delta *= (self.learning_rate / batch_size)

                self.weight_matrices[layer - 1] -= w_delta
                self.bias_matrices[layer - 1] -= b_delta

    def test(self, inputs, round_output=True):
        ''' runs list of inputs through network '''
        correct = [0, 0]

        for ind in range(len(inputs)):
            item, expected = inputs[ind]
            self.__feed_forward(item)
            outputs = self.activation_matrices[-1].A1

            if round_output:
                o = [round(i, 0) for i in outputs]
            else:
                o = [] + outputs

            # checks if output matches expected
            total_wrong = sum([1 for i in range(len(o)) if expected[i] != o[i]])

            if total_wrong == 0:
                correct[0] += 1

            correct[1] += 1
            correct_perct = (correct[0] / correct[1]) * 100

            print("TEST {}, INPUTS: {}, OUTPUT: {}, (%) CORRECT: {}".format(ind, item, o, correct_perct))

    def __feed_forward(self, inputs):
        ''' performs feedforward algorithm
            :param inputs: 1D list of inputs for each input neuron,
                           e.g. [0.23, 0.4534] '''
        # set inputs as input layer activations
        for ind in range(len(inputs)):
            val = inputs[ind]
            self.activation_matrices[0].itemset((ind, 0), val)

        # perform weigh_matrix(L) * activation_matrix(L - 1) + bias(L)
        # for each layer L starting from first hidden layer
        # and update acviation for L with said values
        for layer in range(1, self.no_of_layers):
            # -1 for weight, weighted_inputs and bias as they skip input layer
            weighted_sum_input = self.weight_matrices[layer - 1] * self.activation_matrices[layer - 1] + self.bias_matrices[layer - 1]
            self.weighted_inputs[layer - 1] = weighted_sum_input

            # put weighted sum vector through activaion function
            x = self.__activation_function(weighted_sum_input)
            self.activation_matrices[layer] = x.copy()

    def __backprop(self, expected_results):
        ''' runs backprop algorithm usign quadratic cost function
            :param expected_results: list of actual results for given inputs '''
        # output -> first_hidden
        layer_errors = []

        # output layer error
        # calc gradient of cost function with respect to activations
        # c_grad_a = (a_L - y)
        exp_out_matrix = numpy.matrix([[i] for i in expected_results])
        c_grad_a = self.activation_matrices[-1] - exp_out_matrix
        # c_grad_a = 2 * (self.activation_matrices[-1] - exp_out_matrix)

        # apply deriv of activation function to weighted_input of output layer
        act_deriv = self.__activation_function(self.weighted_inputs[-1], deriv=True)

        # hadamard product
        output_error = numpy.multiply(c_grad_a, act_deriv)

        layer_errors.append(output_error)

        # hidden layers errors
        for layer in range(self.no_of_layers - 2, 0, -1):
            z = self.weighted_inputs[layer - 1]
            w = self.weight_matrices[layer]
            s = layer_errors[-1]

            # apply deriv of activation function to weighted_input of output layer
            act_deriv = self.__activation_function(z, deriv=True)

            wt_s = w.transpose() * s
            error = numpy.multiply(wt_s, act_deriv)

            layer_errors.append(error)

        return layer_errors

    def __activation_function(self, z, deriv=False):
        ''' applies activation function to each item in matrix '''
        new_matrix = []

        for node in z.A1:
            a = self.__act_funtions(node, self.activation_function, deriv)
            new_matrix.append([a])

        return numpy.matrix(new_matrix)

    def __act_funtions(self, x, funct, deriv=False):
        ''' returns activation function used by NN '''
        # sigmoid, guassian,
        functs = {0: lambda x, deriv: functs[0](x, 0) * (1 - functs[0](x, 0)) if deriv else 1 / (1 + math.exp(-x)),
                  1: lambda x, deriv: -2 * x * math.exp(-x**2) if deriv else math.exp(-x**2)}

        return functs[funct](x, deriv)
