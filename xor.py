#!/usr/bin/env python3
from neural_network import NeuralNetwork
from random import randint


''' XOR problem solved using NN library '''

def main():
    layers = [2, 2, 1]
    nn = NeuralNetwork(layers)

    # set activaion to guassian
    nn.activation_function = 1

    # generate XOR data
    data = [([0, 0], [0]), ([0, 1], [1]), ([1, 0], [1]), ([1, 1], [0])]

    nn.train(data, max_epochs=100000)

    #generate XOR data
    data = []

    for i in range(100):
        a = randint(0, 1)
        b = randint(0, 1)

        data.append(([a, b], [a^b]))
    nn.test(data * 1000)

if __name__ == '__main__':
    main()
