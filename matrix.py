#!/usr/bin/env python3
import sys

''' Matrix class will be used to handle matrix maths with the NN, it's therefore
    not comprehensive and only contains arthemtic required in NN '''

class Matrix:
    ''' Represent a matrix object '''

    def __init__(self, rows):
        ''' init matrix, rows is a list of at least one list of numbers
            where a list represents one row, e.g. 3x2 matrix
            [[1, 2], [4, 5], [4, 5]] '''
        self.__validate_rows(rows)
        self.rows = rows
        self.row_len = len(rows)
        self.cols = self.__rows_to_cols(rows)
        self.col_len = len(self.cols)

    def __add__(self, matrix):
        ''' addition operation - this matrix + matrix '''
        if not isinstance(matrix, Matrix):
            print("Can only perform addition with other matrix objects")
            return -1

        if self.row_len != matrix.row_len or self.col_len != matrix.col_len:
            print("To add two matrices they must have the same number of columns and rows.")
            return -1

        new_rows = []

        for r in range(self.row_len):
            row = []

            for item in range(self.col_len):
                a = self.rows[r][item]
                b = matrix.rows[r][item]
                row.append(a + b)

            new_rows.append(row)

        return Matrix(new_rows)

    def __mul__(self, matrix):
        ''' multplies this matrix and matrix passed, returns new matrix '''
        if not isinstance(matrix, Matrix):
            print("Can only multiply by another matrix.")
            sys.exit(-1)

        if self.col_len != matrix.row_len:
            print("Cannot mutliply by this matrix. Number of rows must match number of columns of this matrix.")
            sys.exit(-1)

        new_columns = []

        for i in range(matrix.col_len):
            col = matrix.cols[i]
            item = 0

            new_columns.append([])

            for j in range(self.row_len):
                row = self.rows[j]

                item = sum([col[x] * row[x] for x in range(len(row))])
                new_columns[i].append(item)

        rows = self.__cols_to_rows(new_columns)

        return Matrix(new_columns)

    def __cols_to_rows(self, cols):
        ''' converts cols to rows '''
        row_len = len(cols)

        rows = []

        for i in range(row_len):
            rows.append([])

            for c in cols:
                rows[i].append(c[i])

        return rows

    def __rows_to_cols(self, rows):
        ''' converts rows to cols '''
        col_len = len(rows[0])

        # NOTE: list comprehension is used instead of [[]] * col_len
        #       as that way was causing the item copied to one list to
        #       be copied to all others, e.g. [[], []] doing
        #       list[0].append(1) resulted in [[1], [1]]
        cols = [[] for i in range(col_len)]

        # goes through rows and adds each column to corrposnding
        # column index in cols
        for row in rows:
            for i in range(len(row)):
                cols[i].append(row[i])

        return cols

    def __validate_rows(self, rows):
        ''' checks rows only conains list of numbers '''
        if not rows or not rows[0]:
            raise Exception("rows, and their items, cannot be empty")

        col_len = len(rows[0])

        for row in rows:
            if type(row) != list:
                raise Exception("INVALID ROWS, must be list of numbers.")
            for ind in range(col_len):
                try:
                    item = row[ind]
                    if type(item) != int and type(item) != float:
                        raise Exception("Invalid row items, must be number.")
                except IndexError:
                    raise Exception("Each row must have the same number of columns")

if __name__ == '__main__':
    rows = [[1, 2], [1, 2], [1, 2]]
    x = [[1], [2]]
    a = Matrix(rows)
    b = Matrix(x)

    c = a * b

    print(c.rows)
